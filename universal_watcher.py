#!/usr/bin/python3.6
#****************************************************************************************************
# Universal Watcher Script
# Current Version : v1.00
#
#****************************************************************************************************

import subprocess
import sys
import threading
import configparser
import os
import time
import logging

class universal_watcher:
    cmd = ""
    output = ""
    error = ""
    run = ""

    def setParser(self):
        try:

            cfg = configparser.ConfigParser()
            log.info("Reading configuration file!")
            if os.path.isfile('./watcher.cfg'):
                cfg.read('./watcher.cfg')
            else:
                log.error("No configuration file found!")
                exit(1)
            return cfg

        except IOError as e:
            log.error("No configuration file found! {}".format(e))
            print("THis")
            exit(1)

    def checkProcess(self, name, identifier, location, statrtScript, interval,startdelay):
        while True:
            log.info("Checking processors")
            self.cmd = "ps -ef | grep -w {} | grep -wv grep | wc -l".format(identifier)
            log.debug(self.cmd)
            if int(self.runCommand(self.cmd)) > 0:
                log.info("{} Already running!".format(name))

            else:
                log.warn("{} Not running!".format(name))
                log.info("Running start Script")
                log.debug(os.path.join(location, statrtScript))

                self.execute(os.path.join(location, statrtScript), int(startdelay))
            time.sleep(int(interval))


    def runCommand(self, cmd):
        self.output = ""
        self.error = ""

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.output, self.error = p.communicate()

        if p.returncode != 0:
            log.error("%d %s" % (p.returncode, self.error.decode('utf-8')))
        else:

            return self.output.decode('utf-8')

    def execute(self, component,sleep):

        self.run = subprocess.Popen(['/bin/bash', os.path.expanduser(component)])
        time.sleep(sleep)
        log.info("{} is started.".format(component))

    def getComponents(self, config):
        component = {}
        for section in config.sections():
            component[section] = {}
            try:
                component[section]['location'] = config.get(section, 'location')
            except configparser.NoOptionError as error:
                log.error(error)
            try:
                component[section]['startScript'] = config.get(section, 'startScript')
            except configparser.NoOptionError as error:
                log.error(error)

            try:
                component[section]['identifier'] = config.get(section, 'identifier')
            except configparser.NoOptionError as error:
                log.error(error)

            try:
                component[section]['interval'] = config.get(section, 'interval')
            except configparser.NoOptionError as error:
                log.error(error)
            try:
                component[section]['startdelay'] = config.get(section,'startdelay')
            except configparser.NoOptionError as error:
                log.error(error)
            log.debug("Component read from configuration file %s" % component)
        return component

    def thread(self,component):
        try:
            log.info("Threads starting!")
            threads = []
            for com in component:
                name = com
                location = component[com]['location']
                identifier = component[com]['identifier']
                startScript = component[com]['startScript']
                interval = component[com]['interval']
                startdelay = component[com]['startdelay']
                threads.append(threading.Thread(name=com, target=watcher.checkProcess, args=(name, identifier, location, startScript, interval,startdelay,)))

            for thread in threads:
                thread.start()
        except Exception as e:
            log.error(e)
            exit(1)

    def logger(self):
        ComponentHome = os.getcwd()
        Date = str(time.strftime("%Y%m%d"))
        if not os.path.exists("logs"):
            os.makedirs("logs")

        file = os.path.join(ComponentHome, "logs", "".join(["Universal-Watcher-", Date, ".log"]))
        try:
            # create logger
            logFormatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s',datefmt='%m/%d/%Y %I:%M:%S:%p')
            logger = logging.getLogger('Watcher')
            logger.setLevel(logging.DEBUG)

            # create file handler and set level to debug or info
            fileHandler = logging.FileHandler(file)
            if len(sys.argv) > 1 and sys.argv[1] == "--debug":
                fileHandler.setLevel(logging.DEBUG)
            else:
                fileHandler.setLevel(logging.INFO)
            fileHandler.setFormatter(logFormatter)
            # add ch to logger
            logger.addHandler(fileHandler)
            return logger

        except Exception as e:
            print(e)
            exit(1)


watcher = universal_watcher()
log = watcher.logger()
parser = watcher.setParser()
components = watcher.getComponents(parser)
watcher.thread(components)
